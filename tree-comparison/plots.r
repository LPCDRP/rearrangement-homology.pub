library(ggplot2)
library(scales)
library(gridExtra)
library(forcats)
library(tidyverse)

source("libplot.r")

final_methods = commandArgs(trailingOnly=TRUE)

final_methods = c("SNP",
                  "SibeliaZ+MLWD","SibeliaZ+maf2synteny+MLWD",
                  "Cactus(Mash)+MLWD","Cactus(Mash)+maf2synteny+MLWD", "Cactus(Mash)+maf2synteny+DING",
                  "Cactus(Mash)-filtered+MLWD", "Cactus(Mash)-filtered+DING",
                  "Cactus(SibeliaZ)+MLWD","Cactus(SibeliaZ)+maf2synteny+MLWD",
                  "Cactus(SibeliaZ)-filtered+MLWD", "Cactus(SibeliaZ)-filtered+DING",
                  "Cactus(SNP)+MLWD", "Cactus(SNP)+maf2synteny+MLWD", "Cactus(SNP)+maf2synteny+DING",
                  "Cactus(SNP)-filtered+MLWD", "Cactus(SNP)-filtered+DING",
                  "Cactus(SNP)-filtered+maf2synteny+MLWD", "Cactus(SNP)-filtered+maf2synteny+DING",
                  "annotation+MLWD","annotation+maf2synteny+MLWD",
                  "Cactus(SibeliaZ)+maf2synteny+DING",
                  "SibeliaZ+DING","SibeliaZ+maf2synteny+DING",
                  "annotation+maf2synteny+DING",
                  "Mash"
                  )

stats_o = read.table("summary.tsv", header=TRUE)
stats_o$outgroup = TRUE

stats_noog = read.table("summary.nooutgroup.tsv", header=TRUE)
stats_noog$outgroup = FALSE

stats = rbind(stats_o, stats_noog) %>%
    mutate_at(c("Method", "Reference"), funs(str_replace(.,"-nooutgroup",""))) %>%
    as.data.frame()

incompatibilities = stats[stats$Reference == "SNP" | stats$Reference == "SibeliaZ+maf2synteny+MLWD",
                          c(1,2,3,9,(ncol(stats)-3):ncol(stats))]
incompatibilities_raw = incompatibilities[
    !grepl("+DING|Mash|maf2synteny|Cactus-WGA|Cactus\\(Mash\\)|Cactus\\(SibeliaZ\\)|Cactus\\(2\\)",incompatibilities$Method)
    & (incompatibilities$Method%in%final_methods),]
incompatibilities_maf2synteny = incompatibilities[
    !grepl("+DING|Mash|Cactus-WGA|Cactus\\(Mash\\)|Cactus\\(SibeliaZ\\)|Cactus\\(2\\)",incompatibilities$Method)
    & grepl("maf2synteny",incompatibilities$Method)
    & (incompatibilities$Method%in%final_methods),]

incompatibility_figures = tribble(
    ~name,  ~facetcol, ~data,
    "raw", "outgroup", incompatibilities_raw[incompatibilities_raw$Reference == "SNP",],
    "outgroup.raw",   "Reference", incompatibilities_raw[incompatibilities_raw$outgroup == TRUE,],
    "nooutgroup.raw", "Reference", incompatibilities_raw[incompatibilities_raw$outgroup == FALSE,],
    "maf2synteny", "outgroup", incompatibilities_maf2synteny[incompatibilities_maf2synteny$Reference == "SNP",],
    "outgroup.maf2synteny",   "Reference",  incompatibilities_maf2synteny[incompatibilities_maf2synteny$outgroup == TRUE,],
    "nooutgroup.maf2synteny", "Reference",  incompatibilities_maf2synteny[incompatibilities_maf2synteny$outgroup == FALSE,],
    "annotation-vs-relaxed", "relaxed",
    incompatibilities[
        !incompatibilities$outgroup
        & grepl("annotation", incompatibilities$Method)
        & !grepl("+DING",incompatibilities$Method)
        & incompatibilities$Reference == "SNP",]
    %>% cbind(., data.frame(relaxed=grepl("relaxed",.$Method)))
    %>% mutate_at(c("Method"), funs(str_replace(.,"\\-relaxed",""))),
    "raw-vs-maf2synteny", "maf2synteny",
    incompatibilities[
        !incompatibilities$outgroup
        & incompatibilities$Method%in%final_methods
        & !grepl("+DING|Mash|^Cactus\\(SNP\\)\\+|Cactus-WGA|Cactus\\(Mash\\)|Cactus\\(SibeliaZ\\)|Cactus\\(2\\)",incompatibilities$Method)
        & grepl("SNP",incompatibilities$Reference),]
    %>% cbind(., data.frame(maf2synteny=grepl("maf2synteny",.$Method)))
    %>% mutate_at(c("Method"), funs(str_replace(.,"\\+maf2synteny","")))
    %>% mutate_at(c("Method"), funs(str_replace(.,"Cactus\\(SNP\\)","Cactus")))
    %>% mutate_at(c("Method"), funs(str_replace(.,"SibeliaZ","SibeliaZ-LCB"))),
    "cactus-raw-vs-filtered", "filtered",
    incompatibilities[
        !incompatibilities$outgroup
        & grepl("^Cactus", incompatibilities$Method)
        & !grepl("+DING|^Mash|Cactus-WGA|Cactus\\(2\\)|Pangenome|maf2synteny",incompatibilities$Method)
        & grepl("SNP",incompatibilities$Reference),]
    %>% cbind(., data.frame(filtered=grepl("filtered",.$Method)))
    %>% mutate_at(c("Method"), funs(str_replace(.,"\\-filtered","")))
    %>% mutate_at(c("Method"), funs(str_replace(.,"SibeliaZ","SibeliaZ-LCB"))),
    "cactus-guidetrees.raw", "Reference",
    incompatibilities[
        !grepl("+DING|maf2synteny|filtered",incompatibilities$Method)
        & grepl("Cactus\\(",incompatibilities$Method)
        & !incompatibilities$outgroup,],
#        & grepl("^SNP$",incompatibilities$Reference),],
    "cactus-guidetrees.filtered", "Reference",
    incompatibilities[
        !grepl("+DING",incompatibilities$Method)
        & !grepl("maf2synteny",incompatibilities$Method)
        & !grepl("Mash",incompatibilities$Method)
        & grepl("filtered", incompatibilities$Method)
        & grepl("Cactus\\(",incompatibilities$Method),]
    %>% mutate_at(c("Method"), funs(str_replace(.,"SibeliaZ","SibeliaZ-LCB")))
    %>% mutate_at(c("Reference"), funs(str_replace(.,"SibeliaZ","SibeliaZ-LCB"))),
    "cactus-guidetrees.maf2synteny", "Reference",
    incompatibilities[
        !grepl("+DING",incompatibilities$Method)
        & !grepl("Mash",incompatibilities$Method)
        & grepl("maf2synteny", incompatibilities$Method)
        & grepl("filtered", incompatibilities$Method)
        & grepl("Cactus\\(",incompatibilities$Method)
        & !incompatibilities$outgroup,]
    %>% mutate_at(c("Method"), funs(str_replace(.,"SibeliaZ","SibeliaZ-LCB")))
    %>% mutate_at(c("Reference"), funs(str_replace(.,"SibeliaZ","SibeliaZ-LCB"))),
    "raw.presentation", "Reference",
    incompatibilities_raw[
        incompatibilities_raw$outgroup == FALSE
        & !grepl("SibeliaZ",incompatibilities_raw$Reference),],
    "maf2synteny.presentation", "Reference",
    incompatibilities_maf2synteny[
        incompatibilities_maf2synteny$outgroup == FALSE
        & !grepl("SibeliaZ",incompatibilities_maf2synteny$Reference),],
    "guidetrees.SNP.presentation", "Reference",
    incompatibilities[
        incompatibilities$outgroup == FALSE
        & !grepl("+DING|^Mash",incompatibilities$Method)
        & grepl("maf2synteny", incompatibilities$Method)
        & grepl("Cactus\\(",incompatibilities$Method)
        & grepl("^SNP$",incompatibilities$Reference),],
    "guidetrees.SibeliaZ.presentation", "Reference",
    incompatibilities[
        incompatibilities$outgroup == FALSE
        & !grepl("+DING|^Mash",incompatibilities$Method)
        & grepl("maf2synteny", incompatibilities$Method)
        & grepl("Cactus\\(",incompatibilities$Method)
        & grepl("SibeliaZ",incompatibilities$Reference),],
)



for (fig in incompatibility_figures$name) {
    data = as.data.frame(subset(incompatibility_figures,
                                subset = name  == fig,
                                select = data)[[1]])
    facetcol = subset(incompatibility_figures,
                      subset = name == fig,
                      select = facetcol)[[1]]
    ribbon_plot(data, facetcol = facetcol) +
        theme(legend.position = c(0.20, 0.20), legend.direction = "vertical")
    # for raw-vs-maf2synteny
    ggsave(paste0("incompatibilities.",fig,".pdf"),
           width = 7.0, height = 3.6, device = "pdf")
    fraction_plot(data, facetcol = facetcol)
    ggsave(paste0("incompatibilities-fraction.",fig,".pdf"),
           width = 7.0, height = 3.6, device = "pdf")
}


param_effects = stats[(stats$Method == "Cactus(SNP)+maf2synteny+MLWD" | stats$Method == "Cactus(SNP)-defaults+MLWD") & (stats$Reference == "SNP"),
                      c(1,2,3,9,(ncol(stats)-3):ncol(stats))]
ggplot(param_effects,
       aes(x=minBS/100,
           group=interaction(Method,Reference))) +
    geom_ribbon(
        aes(ymin=(total_splits_in_method + total_splits_in_ref) - (splits_in_method_incompat_with_ref + splits_in_ref_incompat_with_method),
            ymax=total_splits_in_method + total_splits_in_ref,
            fill=Method,
            alpha=0.01)) +
    scale_alpha_continuous(guide="none") +
    geom_line(aes(y=(total_splits_in_method + total_splits_in_ref) - (splits_in_method_incompat_with_ref + splits_in_ref_incompat_with_method),
            color=Method)) +
    geom_line(aes(y=(total_splits_in_method + total_splits_in_ref),
            color=Method)) +
    scale_color_brewer(palette="Dark2",name="") +
    scale_fill_brewer(palette="Dark2",name="") +
    scale_shape(name="") +
    ylab("total number of branches and compatible branches") +
    theme_classic() +
    theme(legend.position = c(.45,.10),legend.direction = "horizontal")+
    scale_x_continuous(name="minimum bootstrap support",labels=percent)
ggsave("incompatibilities_param_effects.pdf",width = 4.5,height = 4.5)


snp = stats[stats$Method == "Cactus-WGA" & stats$Reference == "SNP",
                      c(1,2,3,9,(ncol(stats)-3):ncol(stats))]
ggplot(snp,
       aes(x=minBS/100),
       group=interaction(Method,Reference)) +
    geom_ribbon(
        aes(ymin=(total_splits_in_method + total_splits_in_ref) - (splits_in_method_incompat_with_ref + splits_in_ref_incompat_with_method),
            ymax=total_splits_in_method + total_splits_in_ref,
            fill=Method)) +
    geom_line(aes(y=(total_splits_in_method + total_splits_in_ref) - (splits_in_method_incompat_with_ref + splits_in_ref_incompat_with_method),
            color=Method)) +
    geom_line(aes(y=(total_splits_in_method + total_splits_in_ref),
            color=Method)) +
    scale_color_brewer(palette="Dark2",name="") +
    scale_fill_brewer(palette="Dark2",name="") +
    scale_shape(name="") +
    ylab("total number of branches and compatible branches") +
    theme_classic() +
    theme(legend.position = "none")+
    scale_x_continuous(name="minimum bootstrap support",labels=percent)
ggsave("snp_tree_comparisons.pdf",width = 7.0,height = 3.6)


snp = stats[stats$Reference == "SNP" | stats$Reference == "Cactus-WGA",
                          c(1,2,3,9,(ncol(stats)-3):ncol(stats))]

ggplot(snp[!grepl("+DING",snp$Method) & (snp$Method%in%final_methods),],
       aes(x=minBS/100,
           group=interaction(Method,Reference))) +
    geom_ribbon(
        aes(ymin=(total_splits_in_method + total_splits_in_ref) - (splits_in_method_incompat_with_ref + splits_in_ref_incompat_with_method),
            ymax=total_splits_in_method + total_splits_in_ref,
            fill=Method,
            alpha=0.01)) +
    scale_alpha_continuous(guide="none") +
    geom_line(aes(y=(total_splits_in_method + total_splits_in_ref) - (splits_in_method_incompat_with_ref + splits_in_ref_incompat_with_method),
            color=Method)) +
    geom_line(aes(y=(total_splits_in_method + total_splits_in_ref),
            color=Method)) +
    scale_color_brewer(palette="Dark2",name="") +
    scale_fill_brewer(palette="Dark2",name="") +
    scale_shape(name="") +
    ylab("total number of branches and compatible branches") +
    theme_classic() +
    theme(legend.position = c(.15,.10),legend.direction = "horizontal")+
    guides(color=guide_legend(nrow=3, byrow=FALSE))+
    scale_x_continuous(name="minimum bootstrap support",labels=percent) +
    facet_wrap(~fct_rev(Reference), labeller=label_both)
ggsave("incompatibilities-snp-wga.pdf",width = 7.0,height = 3.6)

ggplot(stats,aes(x=Method,y=Reference,
                 fill=splits_in_method_common_to_ref))+
  theme_classic()+geom_tile()+facet_wrap(~minBS,ncol=2)+
  geom_text(aes(label=splits_in_method_common_to_ref)) +
  scale_fill_gradient2(midpoint=0,high='red',name="Number of shared branches")+
  scale_x_discrete(name="")+
  scale_y_discrete(name="")+
  theme(legend.position = "bottom",legend.margin = margin(0,0,0,0,"pt"),
        legend.box.spacing = unit(0,'pt'),
        axis.text.x = element_text(angle=45, vjust=0.5))
  ggsave("shared-branches.pdf",width=6.2,height = 6.7)


ggplot(stats,aes(x=Method,y=Reference,
                 fill=splits_in_method_incompat_with_ref))+
  theme_classic()+geom_tile()+facet_wrap(~minBS,ncol=2)+
  scale_fill_gradient2(midpoint=0,high='red',name="Number of incompatible branches")+
  scale_x_discrete(name="")+
  scale_y_discrete(name="")+
  theme(legend.position = "bottom",legend.margin = margin(0,0,0,0,"pt"),
        legend.box.spacing = unit(0,'pt'),
        axis.text.x = element_text(angle=45, vjust=0.5))+
  geom_text(aes(label=splits_in_method_incompat_with_ref))+
  scale_color_gradient2(high="yellow", mid="black")
  ggsave("incompatible-branches.pdf",width=6.2,height = 6.7)


summary_methods = final_methods[!grepl('^Cactus\\(SibeliaZ|^Cactus\\(Mash',final_methods)]

summary_distances = stats[
    stats$Reference %in% summary_methods
    & stats$Method %in% summary_methods
    & !stats$outgroup
    & stats$minBS==0,]

RF = ggplot(summary_distances,
            aes(x=Method,y=Reference,fill=robinson_foulds))+
  theme_classic()+geom_tile()+
  coord_fixed(ratio = 0.7)+
  geom_text(aes(label=round(robinson_foulds, digits=2)))+
  scale_fill_gradient2(midpoint=0,high='red',name="Normalized\nRobinson-Foulds Distance")+
  scale_x_discrete(name="")+
  scale_y_discrete(name="")+
  theme(legend.position = "bottom",legend.margin = margin(0,0,0,0,"pt"),
        legend.box.spacing = unit(0,'pt'),
        axis.text.x = element_text(angle=45, vjust=0.5))
ggsave("RF.pdf",width=8.5,height = 6.7)


MS = ggplot(summary_distances,
            aes(x=Method,y=Reference,fill=matching_split))+
  theme_classic()+geom_tile()+
  coord_fixed(ratio = 0.7)+
  geom_text(aes(label=matching_split)) +
  scale_fill_gradient2(midpoint=0,high='red',name="Matching Split Distance")+
  scale_x_discrete(name="")+
  scale_y_discrete(name="")+
  theme(legend.position = "bottom",legend.margin = margin(0,0,0,0,"pt"),
        legend.box.spacing = unit(0,'pt'),
        axis.text.x = element_text(angle=45, vjust=0.5))
  ggsave("MS.pdf",width=8.5,height = 6.7)



cactus_guide_tree_ms = stats[
    #grepl("^Cactus\\(.*\\)(\\-filtered|\\+maf2synteny)|^SNP|^Mash|^SibeliaZ\\+maf2synteny\\+MLWD", stats$Method)
    grepl("^Cactus\\(.*\\)\\-filtered\\+(MLWD|DING)|^SNP|^Mash|^SibeliaZ\\+maf2synteny\\+MLWD", stats$Method)
    & grepl("^Cactus\\(.*\\)\\-filtered\\+(MLWD|DING)|^SNP|^Mash|^SibeliaZ\\+maf2synteny\\+MLWD", stats$Reference)
    & !grepl("CactusPangenome",stats$Method)
    & !grepl("CactusPangenome",stats$Reference)
    & !grepl("Cactus\\(2\\)",stats$Method)
    & !grepl("Cactus\\(2\\)",stats$Reference)
    & !grepl("Cactus-WGA",stats$Method)
    & !grepl("Cactus-WGA",stats$Reference)
    & !stats$outgroup
    & stats$minBS == 0,]


guide_tree_plot = scatterbox(cactus_guide_tree_ms %>%
           mutate_at(c("Reference","Method"), funs(str_replace(.,"SibeliaZ","SibeliaZ-LCB"))) %>%
           mutate_at(.vars = vars("Reference","Method"),
                     .funs = list(~ factor(., levels = c('SNP',
                                                         'Cactus(SNP)-filtered+MLWD',
                                                         'Cactus(SNP)-filtered+DING',
                                                         #'Cactus(SNP)+maf2synteny+MLWD',
                                                         #'Cactus(SNP)+maf2synteny+DING',
                                                         'Mash',
                                                         'Cactus(Mash)-filtered+MLWD',
                                                         'Cactus(Mash)-filtered+DING',
                                                         #'Cactus(Mash)+maf2synteny+MLWD',
                                                         #'Cactus(Mash)+maf2synteny+DING',
                                                         'SibeliaZ-LCB+maf2synteny+MLWD',
                                                         'Cactus(SibeliaZ-LCB)-filtered+MLWD',
                                                         'Cactus(SibeliaZ-LCB)-filtered+DING'
                                                         #'Cactus(SibeliaZ)+maf2synteny+MLWD',
                                                         #'Cactus(SibeliaZ)+maf2synteny+DING'
                                                         )
                                           ))),
           highlight = gsub("SibeliaZ-LCB$","SibeliaZ-LCB+maf2synteny+MLWD",gsub("Cactus\\((.*)\\).*","\\1",Reference)) == Method
           | Reference == gsub("SibeliaZ-LCB$","SibeliaZ-LCB+maf2synteny+MLWD",gsub("Cactus\\((.*)\\).*","\\1",Method))) +
    guides(color=guide_legend(nrow=3)) +
    theme(legend.margin = margin(0,0,0,0), legend.box.margin = margin(-15,0,0,0)) +
    scale_x_discrete(labels=function(x){gsub("\\+maf2synteny","+\nmaf2synteny",gsub("\\-filtered", "-\nfiltered", x))})
# add shading around the x values corresponding to the guide trees
for (guide in c(1,4,7)) {
    guide_tree_plot = guide_tree_plot +
        geom_rect(data = data.frame(xmin=guide-0.25, xmax=guide+0.25, ymin=-Inf, ymax=Inf),
                  aes(xmin=xmin,xmax=xmax, ymin=ymin, ymax=ymax), alpha=0.2, inherit.aes=FALSE)
}

ggsave("cactus_guide_trees.matchingsplits.pdf", width=7.54, height=5)

distance_set = c(
    "annotation+maf2synteny+MLWD", "annotation+maf2synteny+DING",
    "Cactus(SNP)-filtered+MLWD", "Cactus(SNP)-filtered+DING",
    "Cactus(SNP)-filtered+maf2synteny+MLWD", "Cactus(SNP)-filtered+maf2synteny+DING",
    "Cactus-filtered+MLWD", "Cactus-filtered+DING",
    "Cactus-filtered+maf2synteny+MLWD", "Cactus-filtered+maf2synteny+DING",
#    "Cactus(SibeliaZ)+maf2synteny+MLWD", "Cactus(SibeliaZ)+maf2synteny+DING",
#    "Cactus(SibeliaZ)-filtered+MLWD", "Cactus(SibeliaZ)-filtered+DING",
#    "Cactus(Mash)+maf2synteny+MLWD", "Cactus(Mash)+maf2synteny+DING",
#    "Cactus(Mash)-filtered+MLWD", "Cactus(Mash)-filtered+DING",
#    "Cactus(2)+maf2synteny+MLWD", "Cactus(2)+maf2synteny+DING",
    "SibeliaZ-LCB+MLWD", "SibeliaZ-LCB+DING",
    "SibeliaZ-LCB+maf2synteny+MLWD","SibeliaZ-LCB+maf2synteny+DING",
    "SibeliaZ+MLWD", "SibeliaZ+DING",
    "SibeliaZ+maf2synteny+MLWD","SibeliaZ+maf2synteny+DING"
#    "Mash",
#    "Cactus-WGA",
#                  "CactusPangenome+DING","CactusPangenome+maf2synteny+DING",
#                  "annotation-relaxed+maf2synteny+DING",
#    "SNP",
    )

ms_stats = stats[stats$Reference %in% distance_set
                 & !grepl("^SNP$", stats$Method)
                 & stats$Method %in% distance_set
                 & !stats$outgroup
                 & stats$minBS == 0,]

scatterbox(ms_stats %>%
           mutate_at(c("Method","Reference"), funs(str_replace(.,"Cactus\\(SNP\\)","Cactus"))) %>%
           mutate_at(c("Method","Reference"), funs(str_replace(.,"SibeliaZ","SibeliaZ-LCB"))) %>%
           mutate_at(.vars = vars("Reference","Method"),
                     .funs = list(~ factor(., levels = distance_set))),
           highlight = gsub("\\+(MLWD|DING)","",Method) == gsub("\\+(MLWD|DING)","",Reference))


ggsave("MS.scatterbox.pdf", width=11.3, height=5.77)
