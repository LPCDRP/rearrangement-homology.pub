library(ggplot2)
library(ggsci)
library(scales)
library(gridExtra)
library(tidyverse)


ribbon_plot = function(data, facetcol="Reference") {
    data %>%
    # set the facet order by order of appearance in the data
    # https://community.rstudio.com/t/wrangling-the-levels-of-ordered-and-unordered-factors-with-mutate-at/18031
    # https://r4ds.had.co.nz/factors.html#creating-factors
    mutate_at(.vars = vars("Reference","outgroup"),
              .funs = list(~ factor(., levels = unique(.)))) %>%
    ggplot(aes(x=minBS/100,
               group=interaction(Method,Reference))) +
        geom_ribbon(
            aes(ymin=(total_splits_in_method + total_splits_in_ref) - (splits_in_method_incompat_with_ref + splits_in_ref_incompat_with_method),
                ymax=total_splits_in_method + total_splits_in_ref,
                fill=Method),
            alpha=0.25) +
        scale_alpha_continuous(guide="none") +
        geom_line(aes(y=(total_splits_in_method + total_splits_in_ref) - (splits_in_method_incompat_with_ref + splits_in_ref_incompat_with_method),
                      color=Method)) +
        geom_line(aes(y=(total_splits_in_method + total_splits_in_ref),
                      color=Method)) +
        scale_color_brewer(palette="Dark2",name="") +
        scale_fill_brewer(palette="Dark2",name="") +
        scale_shape(name="") +
        ylab("total number of branches and compatible branches") +
        scale_y_continuous(limits=c(0,200), expand = c(0,0)) +
        theme_classic() +
        theme(legend.direction = "horizontal", legend.position = "bottom") +
        #theme(legend.position = c(.15,.10),legend.direction = "horizontal")+
        #guides(color=guide_legend(nrow=3, byrow=FALSE))+
        guides(color=guide_legend(byrow=FALSE))+
        scale_x_continuous(name="minimum bootstrap support",labels=percent) +
        facet_wrap({{facetcol}}, labeller=label_both)
}

fraction_plot = function(data, facetcol="Reference") {
    data %>%
        mutate_at(.vars = vars("Reference","outgroup"),
              .funs = list(~ factor(., levels = unique(.)))) %>%
        ggplot(aes(x=minBS,
                   y=(splits_in_method_incompat_with_ref + splits_in_ref_incompat_with_method)/(total_splits_in_method + total_splits_in_ref),
                   group=interaction(Method,Reference))) +
        geom_line(aes(color=Method)) +
        xlab("minimum bootstrap support") +
        ylab("fraction of incompatible branches") +
        facet_wrap(facetcol, labeller=label_both) +
        scale_x_continuous(limits=c(0,100), expand = c(0,0)) +
        scale_y_continuous(limits=c(0,1), expand = c(0,0)) +
        theme_classic() +
        theme(legend.direction = "horizontal", legend.position = "bottom")
}

scatterbox = function(data, highlight) {
    data %>%
        ggplot(aes(x=Reference,
                   y=matching_split)) +
        geom_point(aes(color=Method,
                       shape={{highlight}},
                       stroke={{highlight}},
                       )) +
        scale_shape_discrete(guide='none') +
        theme_classic() +
        ylab("Matching Split Distance") +
        labs(shape = "Tree Inference Method") +
        scale_x_discrete(labels=function(x){gsub("\\+", "+\n", x)}) +
        scale_color_d3("category20") +
        scale_fill_d3("category20") +
        theme(
            axis.title.x=element_blank(),
            axis.text.x = element_text(angle=45, hjust=1),
            legend.position = "bottom"
        )
}
