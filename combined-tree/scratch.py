#/usr/bin/env python3

import dendropy

snp = dendropy.Tree.get(path="../tree-comparison/bs75/SNP-nooutgroup.tree", schema="newick")
snp.label = "SNP"

cactus = dendropy.Tree.get(path="../tree-comparison/bs75/Cactus(SNP)-nooutgroup-filtered+MLWD.tree", schema="newick")
cactus.label = "Cactus"

x=dendropy.Tree()

x = x.from_bipartition_encoding([y  for i in [snp, cactus] for y in i.encode_bipartitions()], snp.taxon_namespace)

for bipartition in x.bipartition_edge_map:
    label = ''
    for tree in [snp, cactus]:
        try:
            edge_label = tail = None
            tail = tree.bipartition_edge_map[bipartition].tail_node
            if not tail:
                print("NO TAIL: " + str(tree.bipartition_edge_map[bipartition]))
                continue
            edge_label = tail.label
            if not edge_label:
                edge_label = ''
            label += tree.label + ' ' + edge_label + ' '
        except KeyError:
            continue
        x.bipartition_edge_map[bipartition].tail_node.label = label

print(str(x) + ';')
