#!/usr/bin/env python3

import dendropy
import sys

t=dendropy.TreeList.get(file=sys.stdin, schema="newick")

x=dendropy.Tree()

print(str(x.from_bipartition_encoding([y  for i in t for y in i.encode_bipartitions()],t.taxon_namespace))+";")
