library(ggtree)
library(ggbreak)
library(ggpubr)
library(RColorBrewer)
library(scales)

source("../local/include/lineage.r")

tree = read.tree("combined-cactus-snp-75.rooted.tree")
cactus_tree = read.tree("Cactus(SNP)-nooutgroup-filtered+MLWD.rooted.tree")


lineage = load_lineage()

tree$tip.label = sub("Mcanetti","M. canettii",tree$tip.label)

ggtree(tree) %<+% lineage +
    geom_tiplab(size=1.5, geom="text") +
    geom_tippoint(size=0.75, aes(color=main_lineage)) +
    theme(legend.position = c(0.2,0.85),
          legend.title.align = 0.5, legend.title = element_text(size=8),
          legend.key.size = unit(10, 'pt'),
          legend.text=element_text(size=6),
          plot.margin = grid::unit(c(0,0,0,0), "mm")) +
    labs(color="lineage")
ggsave(file="combined-cac-snp-tree.native.pdf",
       width=8.2, height=5)


ggtree(cactus_tree) %<+% lineage +
    geom_tiplab(size=1.5, geom="text") +
    geom_tippoint(size=0.75, aes(color=main_lineage)) +
    geom_nodelab(size=1.5, geom="label",
                 nudge_y = 0.1, nudge_x = -0.000125,
                 label.padding= unit(0.1, "lines")) +
    theme_tree2(axis.text.x = element_text(size=5),
                legend.position="none") +
    xlab("additions/removals per adjacency") +
    theme(axis.title.x = element_text(size=7)) +
    labs(color="lineage")

ggsave(file="Cactus(SNP)-nooutgroup-filtered+MLWD.pdf", width=8.2, height=6)
