selfdir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

include $(selfdir)/config.mk

vpath %.tree \
 snp-phylogeny \
 mlgo \
 distance \

vpath %.dat \
 synteny


.PHONY: all
all: tree-comparison/summary.tsv synteny/summary.tsv ;


tree-comparison/summary.tsv: \
 $(refs:%=%.tree) \
 $(all_methods:%=%+MLWD.tree)
	$(MAKE) -C $(@D) $(@F)

$(refs:%=%.tree):
	$(MAKE) -C snp-phylogeny "$@"

%+MLWD.tree: %.dat
	$(MAKE) -C mlgo "$@"

%+DING.tree: %.dat
	$(MAKE) -C distance "$@"

synteny/summary.tsv: $(all_methods:%=%.dat)
	$(MAKE) -C $(@D) $(@F)

%.dat:
	$(MAKE) -C synteny "$@"
