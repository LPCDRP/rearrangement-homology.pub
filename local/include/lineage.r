library(tidyverse)

load_lineage = function(source = paste0(Sys.getenv("GROUPHOME"), "/metadata/lineage.tbprofiler.tsv")) {

    lineage = read.table(source,
                         sep="\t",
                         header=TRUE,
                         )
    lineage = bind_rows(lineage,
                        data.frame(
                            sample="M. canettii",
                            main_lineage="M. canettii")
                        )
    lineage = mutate_at(lineage, c("main_lineage"), list(~ str_replace(.,"lineage","L")))

    return(lineage)
}
