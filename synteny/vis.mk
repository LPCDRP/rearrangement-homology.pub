include ../config.mk

#500

clade = \
 4-0087 \
 SEA11020068 \
 2-0043 \
 SEA06535 \
 SEA00042 \
 4-0012 \
 SEA17020030

diverse = \
 3-0096 \
 SEA12126 \
 N1176 \
 N1063 \
 SEA11278 \
 4-0087 \
 H37Rv_S1 \
 SEA09167 \
 4-0096

comparison-set = $(diverse)

# set the `range` variable (e.g. range=12342-23455) to get a limited view

all: \
 cactus.svg \
 sibeliaz.svg \
 annotation.svg \
 annotation-relaxed.svg \


cactus.svg: cactus/500/gggenes.tsv
sibeliaz.svg: sibeliaz/500/gggenes.tsv
annotation.svg: annotation/default/gggenes.tsv
annotation-relaxed.svg: annotation/relaxed/gggenes.tsv
%.svg:
	Rscript --vanilla vis.r $< $@ $(range)

# preprocessing is only needed for maf2synteny outputs
preprocess= parse-coords --gggenes |

annotation/default/gggenes.tsv: preprocess=
annotation/relaxed/gggenes.tsv: preprocess=
%/gggenes.tsv: %/blocks_coords.txt
	< $< \
	$(preprocess) \
	grep -e gene $(comparison-set:%=-e % ) \
	> $@
