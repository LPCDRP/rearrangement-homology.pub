library(ggplot2)
library(gggenes)

args = commandArgs(trailingOnly=TRUE)

blocks = read.table(args[1],header=TRUE)

# we have a specific range to look at
if(length(args) == 3) {
    coords = unlist(strsplit(args[3],'-'))
    min = as.numeric(coords[1])
    max = as.numeric(coords[2])

    blocks = subset(blocks,
    ( (start <= max) & (start >= min) ) |
    ( (end   <= max) & (end   >= min) )
    )
}


ggplot(blocks,
       aes(xmin = start,
           xmax = end,
           y = molecule,
           fill = gene)
       ) +
    geom_gene_arrow() +
    facet_wrap(~ molecule, scales="free", ncol = 1) +
    scale_color_brewer() +
    theme_genes()

ggsave(file=args[2])
