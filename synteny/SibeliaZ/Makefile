selfdir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

include $(selfdir)/../../config.mk

VPATH = ../seqs

.SECONDARY:

b ?= 500

flavors =  $(filter SibeliaZ%, $(all_methods))


.PHONY: all
all: permutations summary.tsv

.PHONY: permutations
permutations: $(flavors:%=%.dat)

summary.tsv: genome-sizes.bed $(flavors)
	summarize-maf2synteny -r -g $^ > $@

%+maf2synteny.dat: %+maf2synteny/$(b)/genomes_permutations.txt
	< "$<" tr -d '+' > "$@"

%+maf2synteny/$(b)/genomes_permutations.txt: %/blocks_coords.gff
	maf2synteny -b $(b) -o '$*+maf2synteny' $<

%.dat: %/genomes_permutations.txt
	< "$<" tr -d '+' > "$@"

%/genomes_permutations.txt: %/blocks_coords.gff
	syntement --outdir "$*" "$<"


seqs = $(outgroups:%=seqs/%.fasta) $(isolates:%=seqs/%.fasta)

.SECONDEXPANSION:
%-nooutgroup/blocks_coords.gff: seqs=$(isolates:%=seqs/%.fasta)
# higher values of k, even k=15 as was recommended by the developers for bacteria, resulted in the warning
#
#         WARNING: some alignment blocks were overlapping by at most 34046
#        It is unexpected and might result into nonsense synteny blocks.
#        Please note that this tool currently support alignments
#        produced by either Cactus or SibeliaZ.
%/blocks_coords.gff: $$(seqs)
	sibeliaz -k 15 -o $* $^


masked-seqs: $(samples:%=masked-seqs/%.fasta)
masked-seqs/%.fasta: ../masked-seqs/%.soft.fasta
	sed '1 s/^>.*/>$*/' $< > $@

seqs/%.fasta: %.fasta
	sed '1 s/^>.*/>$*/' $< > $@

