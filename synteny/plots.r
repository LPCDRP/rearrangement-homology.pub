library(ggplot2)
library(tidyverse)

block_summary = read.table("summary.tsv",
                           header=TRUE)
ggplot(block_summary,
       aes(x=n_blocks, y=avg_coverage)) +
    geom_point(aes(color=grepl("maf2synteny",method),
                   shape=!grepl("nooutgroup",method))) +
    facet_wrap(~gsub("-nooutgroup","", vapply(strsplit(block_summary$method, '\\+'), `[`, 1, FUN.VALUE=character(1))),
               ncol=1) +
    theme_classic() +
    theme(legend.position="bottom", legend.box="vertical")

ggsave("block-summaries.pdf")


bs = block_summary[!grepl("nooutgroup|CactusPangenome|Mash|relaxed",block_summary$method),]
ggplot(bs,
       aes(x=n_blocks, y=avg_unique_frac)) +
    geom_point(aes(color=vapply(strsplit(bs$method, '\\+'), `[`, 1, FUN.VALUE=character(1)),
                   shape=grepl("maf2synteny",method))) +
    facet_wrap(~vapply(strsplit(bs$method, '\\+'), `[`, 1, FUN.VALUE=character(1)),
               ncol=1) +
    theme_classic() +
    theme(legend.position="bottom", legend.box="vertical")

ggplot(bs,
       aes(x=vapply(strsplit(method, '\\+'), `[`, 1, FUN.VALUE=character(1)),
           y=avg_unique_frac)) +
    geom_point(aes(shape=grepl("maf2synteny",method))) +
    theme_classic() +
    xlab("Method") +
    ylab("Average Unique Fraction of Blocks") +
    labs(shape = "with maf2synteny") +
    theme(legend.position="bottom", legend.box="vertical")
ggsave("avg-unique-fraction.png")

ggplot(bs,
       aes(x=vapply(strsplit(method, '\\+'), `[`, 1, FUN.VALUE=character(1)),
           y=avg_coverage)) +
    geom_point(aes(shape=grepl("maf2synteny",method))) +
    theme_classic() +
    xlab("Method") +
    ylab("Average Genome Coverage") +
    labs(shape = "with maf2synteny") +
    theme(legend.position="bottom", legend.box="vertical")
ggsave("avg-coverage.png")

ggplot(bs,
       aes(x=vapply(strsplit(method, '\\+'), `[`, 1, FUN.VALUE=character(1)),
           y=n_blocks)) +
    geom_point(aes(shape=grepl("maf2synteny",method))) +
    theme_classic() +
    xlab("Method") +
    ylab("Number of Blocks") +
    labs(shape = "with maf2synteny") +
    theme(legend.position="bottom", legend.box="vertical")
ggsave("n-blocks.png")

pe = read.table("summary.param-effects.tsv",
                header=TRUE)

metrics = c(
    `avg_coverage` = "Average Genome Coverage Fraction",
    `n_blocks` = "Number of Blocks",
    `n_dup_occ` = "Number of Duplicate Block Occurrences"
)

pe %>%
    subset(method != "Cactus(SNP)-nooutgroup") %>%
    mutate_at(c("method"), funs(str_replace(.,"-nooutgroup",""))) %>%
    mutate_at(c("method"), funs(str_replace(.,"\\(SNP\\)",""))) %>%
    mutate_at(c("method"), funs(str_replace(.,"SibeliaZ","SibeliaZ-LCB"))) %>%
    pivot_longer(starts_with(c("n_blocks","avg_coverage","n_dup_occ")),
                 names_to = "quantity", values_to = "value") %>%
    ggplot(aes(x=block_scale,
               y=value,
               group=method)) +
    geom_line(data = . %>% filter(! block_scale %in% c('raw')) %>% transform(block_scale = as.numeric(block_scale)),
              aes(color=method)) +
    geom_point(data = . %>% filter(block_scale %in% c('raw')) %>% transform(block_scale = 0), aes(color=method)) +
#    scale_y_break(c(6500,15000)) +
    facet_wrap(~quantity, scales="free_y", labeller = as_labeller(metrics)) +
#    scale_y_break(c(8500,125000), scales="free") +
    theme_classic() +
    theme(legend.position = c(0.07, 0.08),
          legend.background = element_rect(fill="transparent"),
          legend.title.align = 0.5) +
    scale_y_log10()

ggsave("param-effects.pdf" ,width=8.4, height=6.33)
#  10.3 x 9.91
