#!/bin/bash

#export PATH=$(realpath ../local/bin/):$PATH
path="$1"
dir="$(dirname $path)"
method="$(basename $path)"

sizes="$2"

for b in 50 250 500 1000 2500 5000
do
    make -C "$dir" b=$b "${method}+maf2synteny/$b/genomes_permutations.txt" 1>&2
done

shopt -s lastpipe

summarize-maf2synteny -r -g $sizes "$dir/${method}+maf2synteny"/*/ \
    | while read line
do
    dupe_info=$(count-duplicates "$dir/${method}+maf2synteny/$(echo "$line" | cut -f1)"/genomes_permutations.txt | cut -f2-5)
    echo "$line	$dupe_info";
  done | sort -n -k1
