# Syntenic Blocks by Cactus

The cactus binaries require newer processors, which is not satisfied by all the nodes on alborz.
If you try running on any node, you may get `SIGILL` errors indicating illegal instructions for that node's CPUs.
compute00-03 are known to work.

To load the cactus installation:

```
module use ~aelghrao/modulefiles
module load cactus
```

Then the Makefile can be used as written.
