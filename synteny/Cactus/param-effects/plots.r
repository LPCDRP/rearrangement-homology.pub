library(ggplot2)
library(gggenes)
library(gridExtra)
library(viridis)

data = read.table("summary.tsv",
                  header=TRUE)

nblocks = ggplot(data, aes(x=factor(max_block_len),y=factor(block_sizes),
                 fill=n_blocks)) +
    theme_classic() +
    geom_tile() +
    scale_fill_gradient2(midpoint=0,high='red',
                         name="Number of Synteny Blocks") +
    geom_text(aes(label=n_blocks)) +
    scale_x_discrete(name="Cactus max_block_len") +
    scale_y_discrete(name="maf2synteny block_sizes") +
    theme(legend.position = "top",legend.margin = margin(0,0,0,0,"pt"),
          legend.box.spacing = unit(0,'pt'),
          legend.spacing.x = unit(1,'pt'))

cov = ggplot(data, aes(x=factor(max_block_len),y=factor(block_sizes),
                 fill=avg_coverage)) +
    theme_classic() +
    geom_tile() +
    scale_fill_gradient2(midpoint=0.88,high='red',
                         name="Average Genome Coverage\nof Synteny Blocks",
                         breaks=c(0.90, 0.92, 0.94)) +
    geom_text(size=3, aes(label=round(avg_coverage,digits=4))) +
    scale_x_discrete(name="Cactus max_block_len") +
    scale_y_discrete(name="") +
#    scale_y_discrete(name="maf2synteny block_sizes") +
    theme(legend.position = "top",legend.margin = margin(0,0,0,0,"pt"),
          legend.box.spacing = unit(0,'pt'),
          legend.title = element_text(size=9))




min = 2060000
max = 2140000

blocksA = read.table("mbl-100000_bs-500/500/gggenes.tsv",
                     header=TRUE)
blocksA = subset(blocksA,
( (start <= max) & (start >= min) ) |
( (end   <= max) & (end   >= min) )
)

blocksB = read.table("mbl-1000_bs-500/500/gggenes.tsv",
                     header=TRUE)
blocksB = subset(blocksB,
( (start <= max) & (start >= min) ) |
( (end   <= max) & (end   >= min) )
)


A = ggplot(blocksA,
           aes(xmin = start,
               xmax = end,
               y = gsub(".1$","", molecule),
               fill = factor(gene))
           ) +
    geom_gene_arrow(show.legend=FALSE) +
    facet_wrap(~ molecule, scales="free_y", ncol = 1) +
    scale_fill_viridis(discrete=TRUE) +
    # scale_color_gradient2() +
    xlab("genome position (bp)") +
    ylab("isolate") +
    ggtitle("Cactus max_block_length=100000\nmaf2synteny block_sizes=500") +
    theme_genes() +
    theme(plot.title=element_text(hjust=0.5, size=10),
          axis.text.x = element_text(size=8))


B = ggplot(blocksB,
           aes(xmin = start,
               xmax = end,
               y = gsub(".1$","", molecule),
               fill = factor(gene))
           ) +
    geom_gene_arrow(show.legend=FALSE) +
    facet_wrap(~ molecule, scales="free_y", ncol = 1) +
    scale_fill_viridis(discrete=TRUE) +
#    scale_color_gradient2() +
    xlab("genome position (bp)") +
    ylab("isolate") +
    ggtitle("Cactus max_block_length=1000\nmaf2synteny block_sizes=500") +
    theme_genes() +
    theme(plot.title=element_text(hjust=0.5, size=10),
          axis.text.x = element_text(size=8))



# we want to use the same X ranges for both synteny block plots to be able to better compare them
#Axmin = layer_scales(A)$x$range$range[1]
#Bxmin = layer_scales(B)$x$range$range[1]
#ABxmin = max(Axmin, Bxmin)

#Axmax = layer_scales(A)$x$range$range[2]
#Bxmax = layer_scales(B)$x$range$range[2]
#ABxmax = min(Axmax, Bxmax)

pdf("param-effects.pdf",width=8.4, height=8.4)
grid.arrange(nblocks + labs(tag="A"),
             cov + ylab(NULL) + theme(axis.text.y=element_blank()) + labs(tag="B"),
             B + coord_cartesian(xlim=c(min,max)) + labs(tag="C"),
             A + coord_cartesian(xlim=c(min,max)) + ylab(NULL) + theme(axis.text.y=element_blank()) + labs(tag="D"),
             ncol=2)
dev.off()
