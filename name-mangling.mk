
# undocumented requirements (for MLGO, at least):
# -- genome names must not contain dashes

unmangle_names=	sed 's/_/-/g' | sed 's/H37Rv-S1/H37Rv_S1/'

%.mangled-names.dat: %.dat
	sed -e '/>/s/\-/_/g' -e '/^>/s/\.[0-9]$$//' "$<" > "$@"
