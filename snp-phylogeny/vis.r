#!/usr/bin/env Rscript

library(ggtree)
library(ggbreak)
library(RColorBrewer)
library(scales)
library(tidyverse)

source("../local/include/lineage.r")

args = commandArgs(trailingOnly=TRUE)

if (length(args) > 0) {
    treefile = args[1]
} else {
    treefile = "Cactus-WGA.rooted.tree"
}
tree = read.tree(treefile)

lineage = load_lineage()

tree$tip.label = sub("Mcanetti","M. canettii",tree$tip.label)
# cactus added a .1 suffix to all the names
tree$tip.label = str_remove(tree$tip.label, "\\.1$")

plot = ggtree(tree) %<+% lineage +
    geom_tippoint(size=0.75, aes(color=main_lineage)) +
    # https://yulab-smu.top/treedata-book/faq.html#shrink-outlier-long-branch
    scale_x_continuous(breaks = scales::pretty_breaks(n=3)) +
    theme_tree2() +
    xlab("substitutions/site") +
    labs(color="lineage")
    # ggbreak >= 0.0.3.992 is needed to be able to use legend.position = "bottom"
    # can't use manual legend positioning with ggbreak:
    # https://github.com/YuLab-SMU/ggbreak/blob/master/NEWS.md#ggbreak-003992    
    #  theme(legend.position = c(0.2,0.5)) +

if (!grepl("nooutgroup", treefile)) {
    plot = plot +
        geom_tiplab(size=1.4, geom="text", nudge_x = -0.00004) +
        theme(legend.position = "bottom", axis.title.x = element_text(vjust=27))
    if (grepl("Cactus", treefile)) {
        plot = plot +
            geom_nodelab(size=1, geom="label",
                         nudge_y = 0.1, nudge_x = -0.0000525,
                         label.padding= unit(0.1, "lines")) +
            scale_x_break(c(0.000115,0.0085))
    } else if (grepl("^SNP", treefile)){
        plot = plot +
            geom_nodelab(size=1, geom="label",
                         nudge_y = 0.1, nudge_x = -0.000055,
                         label.padding= unit(0.1, "lines")) +
            scale_x_break(c(0.00005,0.0085))
    }
}else {
    plot = plot +
        geom_nodelab(size=1, geom="label",
                     nudge_y = 0.1,
                     label.padding= unit(0.1, "lines")) +
        geom_tiplab(size=1.4, geom="text") +
        theme(legend.position = "bottom")
}

ggsave(file=gsub(".tree$",".pdf",treefile), width=8, height=6.5)
