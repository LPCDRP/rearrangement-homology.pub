topsrcdir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

isolates := $(shell cat $(topsrcdir)/isolist-manuscript-fin.txt)
outgroups := Mcanetti
# unique strains used to root the tree in absence of the outgroup
altgroups := N0091 N1176 N1063
samples := $(isolates) $(outgroups)

reference := H37Rv-NC_000962.3.fasta

AWK = awk -F '	' -v OFS='	'

export PATH:=$(topsrcdir)/local/bin:$(PATH)

vpath %.bed $(topsrcdir)/local/resources


# final methods
methods = \
 SibeliaZ \
 SibeliaZ+maf2synteny \
 SibeliaZ-nooutgroup \
 SibeliaZ-nooutgroup+maf2synteny \
 annotation \
 annotation+maf2synteny \
 annotation-nooutgroup \
 annotation-nooutgroup+maf2synteny \


all_methods = \
 $(methods) \
 annotation-relaxed \
 annotation-relaxed+maf2synteny \
 annotation-relaxed-nooutgroup \
 annotation-relaxed-nooutgroup+maf2synteny \
 Cactus(Mash) \
 Cactus(Mash)+maf2synteny \
 Cactus(Mash)-filtered \
 Cactus(Mash)-filtered+maf2synteny \
 Cactus(Mash)-nooutgroup \
 Cactus(Mash)-nooutgroup-filtered \
 Cactus(Mash)-nooutgroup-filtered+maf2synteny \
 Cactus(Mash)-nooutgroup+maf2synteny \
 Cactus(SNP) \
 Cactus(SNP)+maf2synteny \
 Cactus(SNP)-filtered \
 Cactus(SNP)-filtered+maf2synteny \
 Cactus(SNP)-nooutgroup \
 Cactus(SNP)-nooutgroup-filtered \
 Cactus(SNP)-nooutgroup-filtered+maf2synteny \
 Cactus(SNP)-nooutgroup+maf2synteny \
 Cactus(SibeliaZ) \
 Cactus(SibeliaZ)+maf2synteny \
 Cactus(SibeliaZ)-filtered \
 Cactus(SibeliaZ)-filtered+maf2synteny \
 Cactus(SibeliaZ)-nooutgroup \
 Cactus(SibeliaZ)-nooutgroup-filtered \
 Cactus(SibeliaZ)-nooutgroup-filtered+maf2synteny \
 Cactus(SibeliaZ)-nooutgroup+maf2synteny \

listquote=$(foreach element,$1,"$(element)")
# https://stackoverflow.com/a/12324443
not-containing = $(foreach v,$2,$(if $(findstring $1,$v),,$v))
containing     = $(foreach v,$2,$(if $(findstring $1,$v),$v,))

# we couldn't build distance trees for everything, but we could for these.
# (sort to remove duplicates)
dist_feasible := $(sort \
 $(call containing,maf2synteny,$(all_methods)) \
 $(call containing,filtered,$(all_methods)) \
 $(filter SibeliaZ%,$(all_methods)) \
)


refs = \
 SNP \
 SNP-nooutgroup \
 Cactus-WGA \
 Cactus-WGA-nooutgroup \
